FROM openjdk:17-jdk-alpine
ENV DB_HOST=mongo
ENV DB_PORT=27017
ENV MY_OPENAI_KEY=${MY_OPENAI_KEY}

WORKDIR /app
COPY /builds/ahmedtaoufiq/target/backend.jar app.jar
EXPOSE 8080

CMD ["java", "-jar", "app.jar"]
